<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $p = 'PasswordHardcoded532';

    dd( Hash::check($p, '$2y$10$PDlH6lh9hg5ciWdVD95iFeW2RLUqchuVA08kwB6DhcxIxtSSAbG2O') );
});
