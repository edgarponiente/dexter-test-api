## How to install

Clone this project and then run

`composer update`

after that

run `composer dump-autoload`

then 

find `.env.example` file in the project then rename it to `.env`

then change the content in `.env` with your database credentials

after that

run `php artisan migrate:refresh --seed`
