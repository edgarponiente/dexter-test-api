<?php

use Illuminate\Database\Seeder;

class PasswordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('passwords')->truncate();

        \App\Models\Password::create([
            'password' => '$2y$10$cR2z9NLWftrmaKZp1MNNq.jpo7UDV11LJrB90ubtQzbtnE1KLMlGy'
        ]);
    }
}
