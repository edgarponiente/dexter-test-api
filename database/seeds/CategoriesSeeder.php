<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('categories')->truncate();

        Category::create([
            'name' => 'Fiction'
        ]);

        Category::create([
            'name' => 'Adventure'
        ]);

        Category::create([
            'name' => 'Thrilling'
        ]);

        Category::create([
            'name' => 'Mystery'
        ]);

        Category::create([
            'name' => 'Gossip'
        ]);
    }
}
