<?php

use App\Models\Article;
use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        DB::table('articles')->truncate();

        for ($i = 0; $i < 4; $i++) {
            $category = \App\Models\Category::orderByRaw('RAND()')->first();

            Article::create([
                'title' => $faker->catchPhrase,
                'short_desc' => $faker->realText(),
                'description' => $faker->realText(),
                'author' => $faker->name(),
                'category_id' => $category->id
            ]);
        }

    }
}
