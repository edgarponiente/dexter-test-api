<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use App\Models\Categories;
use App\Models\Password;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function check()
    {
        $password = Password::first();

        if(Hash::check(request()->input('password'), $password->password)) {
            return response()->json([
                'status' => true,
                'token' => $password->password
            ]);
        }

        return response()->json([
            'status' => false
        ]);
    }
}
